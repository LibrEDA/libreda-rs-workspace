#!/bin/bash

# Merge documentation into LibrEDA/pages and push.

set -e

#cargo doc --workspace --no-deps

REPO=git@codeberg.org:LibrEDA/pages.git
TMP=$(mktemp -d)

function cleanup() {
    rm -rf $TMP
}

trap cleanup EXIT

git -C $TMP clone $REPO

cp -r ./target/doc $TMP/pages/

pushd $TMP/pages/

git add doc
git status
echo "Commit? (Enter=Yes, Ctrl-C=Abort)"
read
git commit -m "Update documentation."
git status
echo "Push? (Enter=Yes, Ctrl-C=Abort)"
read
echo "pushing in 2 seconds..."
sleep 2
git push
popd
