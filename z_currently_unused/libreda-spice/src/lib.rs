/*
 * Copyright (c) 2018-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA
 * (see https://codeberg.org/libreda/iron-shapes).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

extern crate regex;

pub use ast::*;

use regex::Regex;
use std::f64;

//
//#[test]
//fn calculator1() {
////    assert!(spice_parser::TermParser::new().parse("22").is_ok());
////    assert!(spice_parser::TermParser::new().parse("1.").is_ok());
////    assert!(spice_parser::TermParser::new().parse("1.234").is_ok());
////    assert!(spice_parser::TermParser::new().parse("(22)").is_ok());
////    assert!(spice_parser::TermParser::new().parse("((((22))))").is_ok());
////    assert!(spice_parser::TermParser::new().parse("((22)").is_err());
//}
//
//#[test]
//fn resistor_parser() {
////    assert!(spice_parser::NetParser::new().parse("vdd").is_ok());
////    assert!(spice_parser::ResistorParser::new().parse("R1 vdd vss 47;").is_ok());
//
//    let lines = "a\nb\nc".lines();
//    for l in lines {
//        println!("{}", l);
//    }
//}

#[test]
fn test_parse_simple_circuit() -> () {
    parse(r"
.subckt TEST a b z
* comment

R1 vdd vss 47 TEMP=270
.end
");
}

fn parse_subckt(tokens: &Vec<&str>) -> Option<Subckt> {
    if tokens.len() < 1 {
        None
    } else {
        let id = Ident(tokens[0].to_string());
        let terminals = tokens.iter()
            .skip(1)
            .map(|s| Net(s.to_string())).collect();

        Some(
            Subckt {
                id,
                terminals,
                components: vec!(),
            }
        )
    }
}

fn parse_float(s: &str) -> Option<f64> {
    Some(s.parse().unwrap_or(0.0 as f64))
}

/// Parts of a line.
#[derive(Copy, Clone, PartialEq, Debug)]
enum Token {
    Command,
    ComponentId,
    Argument,
    Parameter,
}

/// Parse a line into tokens.
fn tokenize<'a>(line: &'a str) -> Vec<(Token, &'a str)> {
    line.split_whitespace()
        .map(|l| l.trim())
        .filter(|l| l.len() > 0)
        .enumerate().map(|(id, tok)| {
        match id {
            0 => match tok.chars().nth(0).unwrap() {
                '.' => (Token::Command, tok),
                'a'...'z' | 'A'...'Z' => (Token::ComponentId, tok),
                _ => panic!("Unsupported start of line: {}", tok)
            },
            _ => if tok.contains("=") {
                (Token::Parameter, tok)
            } else {
                (Token::Argument, tok)
            }
        }
    }).collect()
}

const R_PATTERN: &str = "R* n1 n2 <value> <mname> <L=length> <W=width> <TEMP=temp>";
const M_PATTERN: &str = "M* n1 n2 <value> <mname> <L=length> <W=width> <TEMP=temp>";

use std::collections::BTreeMap;

struct Arguments<'a> {
    args: Vec<&'a str>,
    optional_args: Vec<&'a str>,
    params: BTreeMap<&'a str, &'a str>,
}

fn parse_resistor(tokens: &[(Token, &str)]) -> Resistor {
    assert!(tokens[0].1.starts_with("R"));
    let args = parse_args(&R_PATTERN, &tokens[1..]);
    let id = tokens[0].1;

    Resistor {
        id: Ident(id.to_string()),
        n1: Net(args["n1"].to_string()),
        n2: Net(args["n2"].to_string()),
        // Resistance in Ohms.
        value: args.get("value").map(|v| parse_float(v).unwrap()),
        // Model name.
        mname: args.get("mname").map(|v| Ident(v.to_string())),
        // Length in meters.
        length: None,
        // Width in meters.
        width: None,
        // Temperature
        temp: None,
    }
}

fn parse_component(tokens: &[(Token, &str)]) -> Component {
    let component_type = tokens[0].1.to_uppercase().chars().nth(0).unwrap();

    match component_type {
        'R' => Component::R(Box::new(parse_resistor(&tokens))),
        x => panic!("Unknown component type: {}", x)
    }
}

fn parse_args<'a>(pattern: &'static str, arg_tokens: &[(Token, &'a str)]) -> BTreeMap<&'a str, &'a str> {
    let mut map: BTreeMap<&'a str, &'a str> = BTreeMap::new();

    enum ArgType {
        Mandatory(&'static str),
        Optional(&'static str),
        Parameter(&'static str, &'static str),
    }

    let arg_patterns: Vec<ArgType> = pattern.split_whitespace()
        .skip(1)
        .map(|p| {
            if p.contains("=") {
                let s: Vec<&'static str> = p.splitn(2, "=").collect();
                ArgType::Parameter(s[0], s[1])
            } else if p.starts_with("<") & &p.ends_with(">") {
                let stripped = &p[1..p.len() - 1];
                ArgType::Optional(stripped)
            } else {
                ArgType::Mandatory(p)
            }
        }).collect();

// Get names of ordered arguments

    let ordered_args: Vec<&&'static str> = arg_patterns.iter().filter_map(|ref p| {
        match p {
            ArgType::Optional(s) | ArgType::Mandatory(s) => Some(s),
            _ => None
        }
    }).collect();

    let mandatory_args: Vec<&&'static str> = arg_patterns.iter().filter_map(|ref p| {
        match p {
            ArgType::Mandatory(s) => Some(s),
            _ => None
        }
    }).collect();

// TODO: check if parameter is expected

//    let mandatory_args: Vec<&'static str> = arg_patterns.iter().filter_map(|&p| {
//        match p {
//            ArgType::Mandatory(&s) => Some(s),
//            _ => None
//        }
//    }).collect();

//    let params = arg_patterns.iter().filter_map(|p| {
//        match p {
//            ArgType::Parameter(&a, &b) => Some((a, b)),
//            _ => None
//        }
//    });
//
//    // Map parameter identifiers to names
//    let param_name_map = BTreeMap::from(params);


    for (idx, pattern) in arg_tokens.iter().enumerate() {
        match pattern {
            (Token::Argument, v) => if idx < ordered_args.len() {
                map.insert(ordered_args[idx], v);
            },
            (Token::Parameter, p) => {
                let s: Vec<&'a str> = p.splitn(2, "=").collect();
                map.insert(s[0], s[1]);
            }
            (t, _) => panic!("Unexpected token type: {:?}", t)
        }
    }

    for a in mandatory_args {
        assert!(map.contains_key(a), format!("Not all mandatory arguments found: {}", a));
    }

    map
}

pub fn parse(file: &str) -> Result<(), String> {
    let skip_line = Regex::new(r"^\s*(\*.*)?$").unwrap();

// TODO: join lines starting with '+'.

    let lines = file
        .lines().enumerate()
        .filter(|(_, l)| !skip_line.is_match(l));

    let tokens = lines
        .map(|(line_num, line)| tokenize(line));

    for t in tokens {
        println!("{:?}", t);

        let first_token = t.first().unwrap();
        if first_token.1.starts_with("R") {
            let arg_map = parse_args(&R_PATTERN, &t[1..]);
            println!("arg_map = {:?}", arg_map);
        }

        match first_token {
            (Token::ComponentId, v) => {
                let c = parse_component(&t);
                println!("{:?}", c);
            }
            _ => ()
        }
    }

    let mut subcircuits: Vec<Subckt> = vec!();
    let mut in_subckt = false;

//    for (line_num, l) in lines {
//        println!("{}", l);
//
//        let tokens: Vec<&str> = l.split_whitespace()
//            .map(|l| l.trim())
//            .filter(|l| l.len() > 0)
//            .collect();
//        println!("{:?}", tokens);
//
//        match tokens[0].to_lowercase().as_ref() {
//            ".subckt" => {
//                assert!(!in_subckt);
//                println!("SUBCIRCUIT");
//                let subckt = parse_subckt(&tokens);
//                match subckt {
//                    Some(s) => subcircuits.push(s),
//                    None => panic!("Failed to parse .subckt on line {}", line_num)
//                };
//                in_subckt = true;
//            }
//            ".module" => {
//                panic!("MODULE NOT SUPPORTED");
//            }
//            ".end" => {
//                assert!(in_subckt);
//                println!("END");
//                in_subckt = false;
//            }
//            &_ => {
//                let c = parse_component(&tokens);
//                assert!(in_subckt);
//                match c {
//                    Some(c) => subcircuits.last().unwrap().components.push(c),
//                    None => panic!("Failed to parse component.")
//                }
//            }
//        }
//    }

    Ok(())
}