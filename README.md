# LibrEDA - Cargo Workspace

This repository bundles together the LibrEDA sub-project into a *cargo workspace*. This simplifies the distribution of all sub-crates without them being pushed on crates.io for every small change. Also, some crates that provide example implemenations are possibly not intended to be published on crates.io.

## Getting started

The [libreda-examples](https://codeberg.org/LibrEDA/libreda-examples) repository provides an implementation of a simple-stupid place-and-route tool based on the framework.

## Documentation

To view the code documentation run:
```sh
cargo doc --open
```
